/* Core */
import axios from 'axios';
import waait from 'waait';

/* Interfaces */
import { WeatherInfo } from '../types/WeatherInfo';

const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

const api = {
    async getWeatherInfo(): Promise<{data: WeatherInfo[]}> {
        const response = await axios.get(`${WEATHER_API_URL}`);
        await waait(1000);

        return response.data;
    },
};

export default api;
