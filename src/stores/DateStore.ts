import { makeAutoObservable } from 'mobx';

class DateStore {
    selectedDayId: null | string = null;

    constructor() {
        makeAutoObservable(this);
    }

    setSelectedDayId(selectedDayId: string) {
        this.selectedDayId = selectedDayId;
    }

    get getSelectedDayId () {
        return this.selectedDayId;
    }
}

export const dateStore = new DateStore();
