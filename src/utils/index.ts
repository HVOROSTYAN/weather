export const getWeekDay = (day: number): string => {
    const dateObj = new Date(day);

    return new Intl.DateTimeFormat(
        'ru',
        { weekday: 'long' }
    ).format(dateObj);
}

export const getCurrentDay = (day: number): string => {
    const dateObj = new Date(day);

    return new Intl.DateTimeFormat(
        'ru',
        { month: 'long', day: 'numeric' }
        ).format(dateObj);
}
