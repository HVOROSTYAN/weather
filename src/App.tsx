/* Core */
import React from 'react';
import { observer } from 'mobx-react-lite';

/* Components */
import { Filters } from './components/Filters/Filters';
import { Head } from './components/Head/Head';
import { CurrentWeather } from './components/CurrentWeather/CurrentWeather';
import { WeeklyWeather } from './components/WeeklyWeather/WeeklyWeather';
import { LoadingScreen } from "./components/LoadingScreen/LoadingScreen";

/* Instruments */
import { dateStore } from './stores/DateStore';
import { getWeekDay, getCurrentDay } from './utils';
import useFetchWeatherInfo from './hooks/useFetchWeatherInfo';

export const App: React.FC = observer(() => {
    const { data: weatherInfo, isLoading } = useFetchWeatherInfo();

    if (isLoading) {
        return <LoadingScreen />;
    }

    if (!weatherInfo) {
        return null;
    }

    const selectedDay = !!dateStore.selectedDayId
        ? weatherInfo.data.find((day) => day.id === dateStore.selectedDayId)
        : weatherInfo.data[0];

    if (!selectedDay) {
        return null;
    }

    return (
        <main>
            <Filters />
            <Head
                weekday={getWeekDay(selectedDay.day)}
                monthDay={getCurrentDay(selectedDay.day)}
                weatherType={selectedDay.type} />
            <CurrentWeather
                temperature={selectedDay.temperature}
                rainProbability={selectedDay.rain_probability}
                humidity={selectedDay.humidity} />
            <WeeklyWeather weatherInfo={weatherInfo.data} />
        </main>
    );
});
