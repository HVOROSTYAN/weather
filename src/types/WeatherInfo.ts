export type WeatherType = 'sunny' | 'cloudy' | 'rainy';

export interface WeatherInfo {
    id: string;
    rain_probability: number;
    humidity: number;
    day: number;
    temperature: number;
    type: WeatherType;
}
