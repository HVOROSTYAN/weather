import { WeatherType } from '../../../types/WeatherInfo';

export interface Props {
    weekday: string;
    monthDay: string;
    weatherType: WeatherType;
}
