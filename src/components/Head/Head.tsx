/* Core */
import React from 'react';
import classNames from 'classnames';

/* Interfaces */
import { Props } from './interface/Props';

export const Head: React.FC<Props> = ({ weekday, monthDay, weatherType }) => {
    const iconClassNames = classNames('icon', weatherType);

    return (
        <div className="head">
            <div className={iconClassNames} />
            <div className="current-date">
                <p>{weekday}</p>
                <span>{monthDay}</span>
            </div>
        </div>
    );
}
