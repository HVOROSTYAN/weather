export interface Props {
    isChecked?: boolean;
    onChange?: () => void;
}
