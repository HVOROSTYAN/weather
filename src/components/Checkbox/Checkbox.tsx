/* Core */
import React from 'react';
import classNames from 'classnames';

/* Interfaces */
import { Props } from './interface/Props';

export const Checkbox: React.FC<Props> = ({ isChecked, onChange, children}) => {
    const computedClassNames = classNames('checkbox', {
        'selected': isChecked,
    });

    return (
        <span
            className={computedClassNames}
            onChange={onChange}>
            { children }
        </span>
    );
};
