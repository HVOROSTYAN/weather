/* Core */
import React from 'react';

/* Interfaces */
import { Props } from './interface/Props';

export const CurrentWeather: React.FC<Props> = ({ temperature, rainProbability, humidity }) => (
    <div className="current-weather">
        <p className="temperature">{temperature}</p>
        <p className="meta">
            <span className="rainy">%{rainProbability}</span>
            <span className="humidity">%{humidity}</span>
        </p>
    </div>
);
