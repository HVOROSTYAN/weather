import { WeatherInfo } from '../../../types/WeatherInfo';

export interface Props extends Pick<WeatherInfo, 'temperature' | 'humidity'> {
    rainProbability: WeatherInfo['rain_probability'];
}
