/* Core */
import React from 'react';
import { observer } from 'mobx-react-lite';

/* Components */
import { DayWeather } from '../DayWeather/DayWeather';

/* Instruments */
import { dateStore } from '../../stores/DateStore';

/* Interfaces */
import { Props } from './interface/Props';

export const WeeklyWeather: React.FC<Props> = observer(({ weatherInfo }) => {
    const onDayClick = React.useCallback((id: string): void => {
        dateStore.setSelectedDayId(id);
    }, [dateStore.selectedDayId]);

    const weekWeatherInfo = weatherInfo.slice(0, 7);

    return (
        <div className="forecast">
            {
                weekWeatherInfo.map((dayWeather, index) => {
                    return (
                        <DayWeather
                            key={dayWeather.id}
                            id={dayWeather.id}
                            day={dayWeather.day}
                            isSelected={dateStore.selectedDayId ? dateStore.selectedDayId === dayWeather.id : index === 0}
                            onClick={onDayClick}
                            weather={{
                                temperature: dayWeather.temperature,
                                type: dayWeather.type,
                            }} />
                    )
                })
            }
        </div>
    );
});
