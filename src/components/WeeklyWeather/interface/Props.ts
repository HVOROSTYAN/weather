import { WeatherInfo } from '../../../types/WeatherInfo';

export interface Props {
    weatherInfo: WeatherInfo[];
}
