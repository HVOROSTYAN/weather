/* Core */
import React from 'react';

/* Components */
import { Checkbox } from '../Checkbox/Checkbox';
import { TextField } from '../TextField/TextField';

export const Filters: React.FC = () => (
    <div className="filter">
        <Checkbox>
            Облачно
        </Checkbox>
        <Checkbox isChecked>
            Солнечно
        </Checkbox>
        <TextField
            id="min-temperature"
            label="Минимальная температура" />
        <TextField
            id="max-temperature"
            label="Максимальная температура" />
        <button type="button">
            Отфильтровать
        </button>
    </div>
);
