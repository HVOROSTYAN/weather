/* Core */
import React from 'react';

export interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
    id: string;
    label: string;
    onChange?: (event: React.ChangeEvent<HTMLInputElement & HTMLTextAreaElement>) => void;
}
