/* Core */
import React from 'react';

/* Interfaces */
import { Props } from './interface/Props';

export const TextField: React.FC<Props> = ({ id, label , onChange}) => (
    <div className="custom-input">
        <label htmlFor={id}>
            { label }
        </label>
        <input
            id={id}
            onChange={onChange}
            type="text" />
    </div>
);
