/* Core */
import React from 'react';

/* Components */
import loaderGifSrc from '../../theme/assets/loader.gif';

export const LoadingScreen: React.FC = () => (
    <div className="loading-screen">
        <img
            className="loading-screen__illustration"
            src={loaderGifSrc}
            alt="Loading screen Illustration"/>
    </div>
);
