/* Core */
import React from 'react';
import classNames from 'classnames';

/* Instruments */
import { getWeekDay } from '../../utils';

/* Interfaces */
import { Props } from './interface/Props';

export const DayWeather: React.FC<Props> = ({
    id,
    day,
    weather,
    isSelected,
    onClick,
}) => {
    const computedClassNames = classNames('day', weather.type, {
        'selected': isSelected,
    });

    const handleClick = () => {
        onClick(id);
    }

    return (
        <div
            className={computedClassNames}
            onClick={handleClick}>
            <p>{getWeekDay(day)}</p>
            <span>{weather.temperature}</span>
        </div>
    );
}
