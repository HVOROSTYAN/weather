import { WeatherType } from '../../../types/WeatherInfo';

export interface Props {
    id: string;
    day: number;
    weather: {
        temperature: number;
        type: WeatherType;
    };
    isSelected?: boolean;
    onClick: (id: string) => void;
}
