/* Core */
import { render } from 'react-dom';
import { QueryClientProvider } from 'react-query';

/* Components */
import { App } from './App';

/* Instruments */
import './theme/index.scss';
import { queryClient } from './hooks/queryClient';

render(
    <QueryClientProvider client={queryClient}>
        <App />
    </QueryClientProvider>
    , document.getElementById('root')
);
