/* Core */
import { useQuery } from 'react-query';

/* Instruments */
import api from '../api/api';

const useFetchWeatherInfo = () => {
    return useQuery('weatherInfo', api.getWeatherInfo);
};

export default useFetchWeatherInfo;
